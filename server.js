const express = require("express");
const app = express();
const port = 5000;
const bodyParser = require("body-parser");
const mysql = require("mysql");

const connection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'belajar_express'
});

connection.connect();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.get('/', (request, response)=>{
	response.send("<h1>Hello World</h1>");
});

app.get('/api/users', (request, response)=>{
	let sql = "SELECT * FROM users";
	connection.query(sql, (err, rows, fields)=>{
		if(!err){
			response.json({
				status: 200,
				data: rows,
				message: "Berhasil mengambil data users"
			});
		}
		else{
			console.log(err);
		}
	});
});

app.get('/api/users/:id', (request, response)=>{
	let sql = "SELECT * FROM users WHERE id="+request.params.id;
	connection.query(sql, (err, rows, fields)=>{
		if(!err){
			response.json({
				status: 200,
				data: rows.filter(user=>user.id == request.params.id),
				message: "Berhasil mengambil data users ke-" + request.params.id 
			});
		}
		else{
			console.log(err);
		}
	});
});

app.post('/api/users', (request, response)=>{

	let sql = "INSERT INTO users SET ?";
	let sql2 = "SELECT * FROM users";

	connection.query(sql, request.body, (err, rows, fields)=>{
		connection.query(sql2, (err, rows2, fields)=>{
			if(!err){
				response.json({
					status: 200,
					data: rows2,
					message: "Berhasil post data users"
				});
			}
			else{
				console.log(err);
			}
		});
	});

	
});

app.put('/api/users/:id', (request, response)=>{
	let sql = "UPDATE users SET ? WHERE id=" + request.params.id;
	let sql2 = "SELECT * FROM users";

	connection.query(sql, request.body, (err, rows, fields)=>{
		connection.query(sql2, (err, rows2, fields)=>{
			if(!err){
				response.json({
					status: 200,
					data: rows2,
					message: "Berhasil put data users"
				});
			}
			else{
				console.log(err);
			}
		});
	});

	
});

app.delete('/api/users/:id', (request, response)=>{
	let sql = "DELETE FROM users WHERE id=" + request.params.id;
	let sql2 = "SELECT * FROM users";

	connection.query(sql, (err, rows, fields)=>{
		connection.query(sql2, (err, rows2, fields)=>{
			if(!err){
				response.json({
					status: 200,
					data: rows2,
					message: "Berhasil delete data users ke-" + request.params.id
				});
			}
			else{
				console.log(err);
			}
		});
	});
});

app.listen(port, ()=>{
	console.log(`Server running on localhost:${port}`);
})